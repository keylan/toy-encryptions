//Keylan Petty, Rust Prog 2, OCt 22
//This library crate is designed to encrypt, decrypt, and generate keys using the RSA method

/// Fixed RSA encryption exponent.
use toy_rsa_lib::*;

pub const EXP: u64 = 65537;

/// Generate a pair of primes in the range `2**31..2**32`
/// suitable for RSA encryption with exponent.
pub fn genkey() -> (u32, u32) {
    let mut p: u32 = 1;
    let mut q: u32 = 1;
    while !(EXP < lcm(p as u64, q as u64) && (gcd(EXP, lcm(p as u64, q as u64)) == 1)) {
        p = rsa_prime();
        q = rsa_prime();
    }
    (p, q)
}

/// Encrypt the plaintext `msg` using the RSA public `key`
/// and return the ciphertext.
pub fn encrypt(key: u64, msg: u32) -> u64 {
    modexp(msg as u64, EXP, key)
}

/// Decrypt the cipertext `msg` using the RSA private `key`
/// and return the resulting plaintext.
pub fn decrypt(key: (u32, u32), msg: u64) -> u32 {
    //add inverse funct
    let d = modinverse(EXP, lcm((key.0 - 1) as u64, (key.1 - 1) as u64));
    u32::try_from(modexp(msg, d, key.0 as u64 * key.1 as u64)).unwrap()
}


/* 
#[test]
fn testing_eq() {
    assert_eq!(decrypt((0xed23e6cd,0xf050a04d), 0x12345f),0x12345f);
    assert_eq!(0x6418280e0c4d7675, encrypt(0xed23e6cd as u64 * 0xf050a04d as u64, 0x12345f as u32));
}
*/
