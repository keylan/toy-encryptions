## Keylan Petty, Toy RSA, Rust Programming

##  How to Run
Start with your usual, run of the mill, cargo run on the command line once in the src folder. From there
you can input any hex number(as of right now) and get a generated key. 

## What I did and how it went
This project was created for the purpose of fulfilling assignment requirements for Bart Massey's Rust Programming 
course.
